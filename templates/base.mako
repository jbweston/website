<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link href="/common/kwant.css" rel="stylesheet" type="text/css" />
<title><%block name="title">Kwant: quantum transport simulation package</%block></title>
<link rel="icon" href="/common/kwant_icon.png" type="image/png" />
<%block name="extra_head" />\
</head>

<body>
<div class="padding">
<div id="header" class="meta">
  <a href="/"><img alt="kwant_logo.png" src="/common/kwant_logo.png" /></a>
<h3 class="hidden_structure">Navigation</h3>
<ul class="navigation">
  <li><a href="/">home</a></li>
  <li><a href="/install.html">install</a></li>
  <li><a href="/doc/">documentation</a></li>
  <li><a href="/community.html">community</a></li>
  <li><a href="/authors.html">authors</a></li>
  <li><a href="/citing.html">citing</a></li>
</ul>
</div>

${next.body()}\
<%block name="tail"></%block>\
</div>

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
  _paq.push(["setCookieDomain", "*.kwant-project.org"]);
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);

  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://piwik.kwant-project.org/";
    _paq.push(["setTrackerUrl", u+"piwik.php"]);
    _paq.push(["setSiteId", "1"]);
    var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
    g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><img src="http://piwik.kwant-project.org/piwik.php?idsite=1&amp;rec=1" style="border:0" alt="" /></noscript>
<!-- End Piwik Code: Analytics are used by us to improve the site, and not sent to anyone. -->
</body>
</html>
