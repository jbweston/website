import logging
import docutils.core, docutils.nodes, docutils.io, docutils as du

log = logging.getLogger(__name__)

class Filter(object):
    def __call__(self, data, meta, name, catalog):
        doctree = du.core.publish_doctree(data)
        parts = du.core.publish_parts(
            doctree, reader_name='doctree', source_class=du.io.DocTreeInput,
            writer_name='html', settings_overrides={'initial_header_level': 2})
        data = parts['html_body']
        meta.setdefault('title', parts['title'])
        meta.setdefault('subtitle', parts['subtitle'])
        return data, meta
