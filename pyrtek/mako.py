import logging

log = logging.getLogger(__name__)

class Filter(object):
    def __init__(self, lookup):
        self.lookup = lookup

    def __call__(self, data, meta, name, catalog):
        try:
            template_name = meta['__template__']
        except KeyError:
            log.error('{0}: No __template__ defined.'.format(name))
            return None, meta

        # TODO: surround with try-block
        template = self.lookup.get_template(template_name)
        data = template.render(body=data, name=name, meta=meta, catalog=catalog)

        return data, meta
